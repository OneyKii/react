import react, { useState, useRef, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const [kebabs, setKebabs] = useState([]);
  const [client, setClient] = useState(undefined);

  console.table(kebabs);

  const viande = useRef(null);
  const sauce = useRef(null);
  const sto = useRef(null);

  const kebab = {
    viande: undefined,
    sauce: undefined,
    STO: undefined,
  };

  useEffect(function() {
   
    const newKebabs = kebabs.map((kebab) => {
      const duplicatedKebab = Object.assign({}, kebab);

      duplicatedKebab.viande = kebab.viande[0].toUpperCase() + kebab.viande.slice(1);
      duplicatedKebab.sauce = kebab.sauce[0].toUpperCase() + kebab.sauce.slice(1);

      return duplicatedKebab;
    })

    console.log(kebabs);
    if(JSON.stringify(newKebabs) === JSON.stringify(kebabs)) return;

    setKebabs(newKebabs);
  }, [kebabs])

  function changeClientValue(value) {
    setClient(value);
  }

  function handleSubmitKebab(e) {
    e.preventDefault();
    const newKebab = Object.assign({}, kebab, {
      viande: viande.current.value,
      sauce: sauce.current.value,
      STO: sto.current.checked
    });

    setKebabs([
      ...kebabs,
      newKebab
    ]);
    
    resetForm();
  }

  function resetForm() {
    viande.current.value = null;
    sauce.current.value = null;
    sto.current.checked = false;
  }

  return (
    <>
      {client && <h1> Commande pour : {client} </h1>}

      <input
        name="client"
        onChange={(e) => changeClientValue(e.target.value)}
      />

      <div>
        <h2>Commande de kebab</h2>

        <form onSubmit={handleSubmitKebab}>
          <select ref={viande} name="viande">
            <option disabled selected default>
              Votre viande
            </option>
            <option value="kebab">
              Kebab
            </option>
            <option value="kefta">
              Kefta
            </option>
            <option value="steak">
              Steak
            </option>
            <option value="cordon bleu">
              Cordon bleu
            </option>
            <option value="nuggets">
              Nuggets
            </option>
          </select>
          <select ref={sauce} name="sauce">
            <option disabled selected >
              Sauce
            </option>
            <option value="samouraï">
              Samouraï
            </option>
            <option value="blanche">
              blanche
            </option>
          </select>
          <label>
            Salade tomate oignon chef ?
            <input ref={sto} name="STO" type="checkbox" />
          </label>

          <button type="submit"> Valider </button>
        </form>
      </div>

      <div>
        <h2>Recap commande</h2>
        {
          kebabs.length > 0 && kebabs.map((kebab, i) => {
            console.log(kebab);
            return <KebabItem {...kebab} key={i} />
          })
        }

      </div>
    </>
  );
}

function KebabItem({viande, sauce, STO}) {
  return (
    <ul>
      <li>Viande : {viande}</li>
      <li>Sauce : {sauce}</li>
      <li>Salade Tomate Oignon : {STO ? "✔" : "❌"}</li>
    </ul>
  )
}

export default App;