
import './App.css';
import studentData from './student.json';
import { useState } from 'react';



function App() {

  const [students, setStudents] = useState(studentData);
  const [searchStudents, setSearchStudents] = useState("");


  return (
    <div class="container">
      <h1>Liste des apprenants</h1>
      <input type="text" 
            name="searchBar" 
            placeholder="Rechercher"
            onChange={(event) => {setSearchStudents(event.target.value)}}
      />
      <table>
          <tr>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Promotion</th>
            <th>Compétence</th>
          </tr>
      { studentData.filter((students) => {
        const jsonStringifyStudents = JSON.stringify(students, ['prenom', 'nom', 'competences', 'promotion'])
           if (searchStudents == ""){
             return students
           } else if (jsonStringifyStudents.toLowerCase().includes(searchStudents.toLowerCase())){
             return students
           }}).map(student => { 
        return (<Student {...student} />)
       }) }
      { console.log(students) }
      </table>
    </div>
  );
}
/* props.competences */
function Student(props) {
  return (
    <>
      <tr>
          <td>{ props.prenom }</td>
          <td>{ props.nom }</td>
          <td>{ props.promotion }</td>
              { props.competences.map((competences) => { 
                return <td>{ competences }</td> 
              })}
      </tr>
      
    </>
  )
}



export default App;
