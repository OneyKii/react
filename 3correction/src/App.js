import react, { useState, useRef, useEffect } from "react";
import apprenants from './apprenants.json';
import "./App.css";

function App() {
  const [students, setStudents] = useState(apprenants);
  const [recherche, setRecherche] = useState('');
  
  useEffect(() => {
    if(recherche.length < 3 && recherche.length !== 0 ) return;

    const filteredApprenants = apprenants.filter((apprenant) => {
      return JSON.stringify(apprenant).toLocaleLowerCase().includes(recherche.toLocaleLowerCase());
    })

    setStudents(filteredApprenants);
  }, [recherche])

  return (
    <>
      <h1>Trombinoscope Simplon Campus26</h1>
      <input placeholder="Recherche" value={recherche} onChange={(e) => setRecherche(e.target.value)} />
      <div>
        {students && students.length > 0 && students.map((student) => {
          return <StudentItem {...student} key={student.id} />
        })}
      </div>
    </>
  );
}

function StudentItem({nom, prenom, promotion, competences}) {
  return (
    <ul>
      <li>Nom: {nom}</li>
      <li>PrÃ©nom: {prenom}</li>
      <li>Promotion: {promotion}</li>
      {competences && competences.map((competence, i) => {
        return <li key={i}>{competence}</li>
      })}
    </ul>
  )
}



export default App;