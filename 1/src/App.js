import './css/App.css';
import './css/cards.css';
import { Cards } from './cards/Cards';
import React, { useState, useEffect } from 'react';

import { useForm } from "react-hook-form";

const cards = [
	{id: 1,	state: 'inactive',citation:"On considère que les neuf dixièmes du code correspondent à environ 90% du temps de développement. Les 10% restant correspondent également à 90% du temps de développement", auteur:" Tom Cargill"},
	{id: 2,	state: 'inactive',citation:"Vous ne pouvez pas comprendre la récursivité sans avoir d’abord compris la récursivité", auteur:" Auteur Inconnu."},
	{id: 3,	state: 'inactive',citation:"J’ai toujours rêvé que mon ordinateur soit aussi simple à utiliser que mon téléphone. Ce rêve est devenu réalité : je ne comprends plus comment utiliser mon téléphone", auteur:" Bjarne Stroustrup"},
	{id: 4,	state: 'inactive',citation:"Un ordinateur vous permet de faire plus de bêtises, beaucoup plus rapidement, que n’importe quelle autre invention dans l’histoire de l’humanité. À l’exception notable des armes à feu et de la tequila", auteur:" Mitch Ratcliffe"},
	{id: 5,	state: 'inactive',citation:"Il y existe deux manières de concevoir un logiciel. La première, c’est de le faire si simple qu’il est évident qu’il ne présente aucun problème. La seconde, c’est de le faire si compliqué qu’il ne présente aucun problème évident. La première méthode est de loin la plus complexe", auteur:" C.A.R. Hoare"},
	{id: 6,	state: 'inactive',citation:"Le fossé séparant théorie et pratique est moins large en théorie qu’il ne l’est en pratique", auteur:" Auteur Inconnu"},
	{id: 7,	state: 'inactive',citation:"Si les ouvriers construisaient les bâtiments comme les développeurs écrivent leurs programmes, le premier pivert venu aurait détruit toute civilisation", auteur:" Gerald Weinberg"},
	{id: 8,	state: 'inactive',citation:"Si debugger, c’est supprimer des bugs, alors programmer ne peut être que les ajouter", auteur:" Edsger Dijkstra"},
	{id: 9,	state: 'inactive',citation:"Mesurer la progression du développement d’un logiciel à l’aune de ses lignes de code revient à mesurer la progression de la construction d’un avion à l’aune de son poids", auteur:" Bill Gates"},
	{id: 10,state: 'inactive',citation:"Neuf femmes ne peuvent pas faire un bébé en un mois", auteur:" Fred Brooks"},
	{id: 11,state: 'inactive',citation:"Aujourd’hui, la programmation est devenue une course entre le développeur, qui s’efforce de produire de meilleures applications à l’épreuve des imbéciles et l’univers, qui s’efforce de produire de meilleurs imbéciles. Pour l’instant, l’univers a une bonne longueur d’avance", auteur:" Rich Cook"},
	{id: 12,state: 'inactive',citation:"Les deux principales inventions sorties de Berkeley sont UNIX et le LSD. Difficile de croire à une quelconque coïncidence", auteur:" Jeremy S. Anderson"},
	{id: 13,state: 'inactive',citation:"Avant de vouloir qu’un logiciel soit réutilisable, il faudrait d’abord qu’il ait été utilisable", auteur:" Ralph Johnson"},	
	{id: 14,state: 'inactive',citation:"L'enseignement de l'informatique ne peut faire de personne un programmeur expert plus que l'étude des pinceaux et du pigment peut faire de quelqu'un un peintre expert.", auteur:"Eric S. Raymond"},
	{id: 15,state: 'inactive',citation:"Je m'en fous si ça marche sur votre machine ! Nous ne livrons pas votre machine !", auteur:"Vidiu Platon"},
	{id: 16,state: 'inactive',citation:"Marcher sur l'eau et développer un logiciel à partir d'une spécification sont faciles si les deux sont gelés.", auteur:"Edward V. Berard"},
	{id: 17,state: 'inactive',citation:"Codez comme si le gars qui finit par maintenir votre code est un psychopathe violent qui sait où vous vivez.", auteur:"Jeff Attwood"},
	{id: 18,state: 'inactive',citation:"N’importe quel idiot peut écrire du code qu'un ordinateur peut comprendre. Les bons programmeurs écrivent du code que les humains peuvent comprendre.", auteur:"Martin Fowler"},
	{id: 19,state: 'inactive',citation:"Ajouter des personnes à un projet en retard accroît son retard", auteur:"Frederick Brooks"},
	{id: 20,state: 'inactive',citation:"N'importe quel code que vous avez écrit depuis 6 mois ou plus sans y regarder pourrait tout aussi bien avoir été écrit par quelqu'un d'autre", auteur:"Alan Eagleson"},
	{id: 21,state: 'inactive',citation:"Cookie : Anciennement petit gâteau sucré, qu'on acceptait avec plaisir. Aujourd'hui : petit fichier informatique drôlement salé, qu'il faut refuser avec véhémence.", auteur:"Luc Fayard"},
	{id: 22,state: 'inactive',citation:"Le principe de l'évolution est beaucoup plus rapide en informatique que chez le bipède.", auteur:"Jean Dion"},
	{id: 23,state: 'inactive',citation:"L'informatique, ça fait gagner beaucoup de temps... à condition d'en avoir beaucoup devant soi !", auteur:"Mireille Sitbon"},
	{id: 24,state: 'inactive',citation:"Un programme informatique fait ce que vous lui avez dit de faire, pas ce que vous voulez qu'il fasse.", auteur:"Loi de Murphy"},
	
];

function App() {
	 // On Déclare une nouvelle variable d'état, que l'on va appeler « stateCards »
	const [
			stateCards			// attribut correspondant à l'état de nos cartes 
			,setStateCards		// Fonction Setter permettant de modifier les objets carte
		  ] = useState(cards);
	
	function switchCard(card, click) {

		const idDay = document.getElementById('day');
		console.table(stateCards);

		card.state="active";

		const newStateCards = [];
		stateCards.forEach(item => {
			if(card.id>item.id){
				item.state = "active";
			}
			if(card.id==item.id){
				item.state = "active current";
			}
			newStateCards.push(item);
		})

		setStateCards(newStateCards);
	
	}	



	/* npm install react-hook-form */

	/* @source : https://react-hook-form.com/api/useform */
		
	return (
		<div className="App">
			<header>
				<h1>Developper quotes -- XMAS Calendar</h1>
			</header>
			<div className="form_day">
					<form>
						<input type="number" id="day" name="day" value=""></input>
						<input type="submit" value="Afficher"></input>
					</form>
			</div>
			<div className="cards_list">
				{stateCards.map((item) => {
					return (
						<Cards
							id={item.id}							
							state={item.state}
							texte={item.citation}
							auteur={item.auteur}
							switchCard={switchCard}
							item={item}
						/>
					);
				})}
			</div>
		</div>
	);
}

export default App;